alpha=0.1;
epsilon=1e-2;
Npoints=1e4;
z0=[0;0;0];
addpath('..')
%
axis_font_size=8; height=6;
mycolor=0.9*[1,1,0];
myalpha=0.4;
%
alpha=0.9;
Npoints=1e6;
epsilon=5e-6;
marker_size=3;
z=sample_path(z0,alpha,Npoints,epsilon);
plot3(z(:,1),z(:,2),z(:,3),'.','MarkerSize',marker_size)
hold on
s=plot3(z(:,1),z(:,2),z(:,3),'-','Color',mycolor)
s.Color(4)=myalpha
xlabel('$x$','interpreter','latex')
ylabel('$y$','interpreter','latex')
zlabel('$z$','interpreter','latex')
set(gca,'FontSize',axis_font_size)
filename='test5_3d_alpha0_9.pdf'
adjustpdfpage(gcf,height) %cm height
print(filename,'-dpdf','-r300')
hold off;

%
alpha=1.8;
Npoints=3e5;
epsilon=1e-3;
marker_size=1;

z=sample_path(z0,alpha,Npoints,epsilon);
plot3(z(:,1),z(:,2),z(:,3),'.','MarkerSize',marker_size)
hold on;
s=plot3(z(:,1),z(:,2),z(:,3),'-','Color',mycolor)

s.Color(4)=myalpha
xlabel('$x$','interpreter','latex')
ylabel('$y$','interpreter','latex')
zlabel('$z$','interpreter','latex')
set(gca,'FontSize',axis_font_size)
filename='test5_3d_alpha1_8.pdf'
adjustpdfpage(gcf,height) %cm height
print(filename,'-dpdf','-r300')
hold off;
