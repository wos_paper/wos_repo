function [ out ] = d_ball_0_1( x )
% [out]=d_ball_0_1(x)
%
% Calculates the distance of points inside the domain to the domain boundary
% Domain is a ball of radius 1 centred at 0 in R^n
% Note: This function is vectorised
%
% Inputs
% x: point(s) in R^n for which distance is computed

R=1;
[n,m]=size(x);
O=zeros(n,m);
out = R-sqrt(sum((x-O).^2));

end

