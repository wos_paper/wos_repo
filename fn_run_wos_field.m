function [mean_out, covariance_out,grid]...
  =fn_run_wos_field(grid, params, mean_in, covariance_in, mean_prior, cov_prior,...
    no_samples, method, method_params)
   % function [mean_out, covarsciance_out]=fn_run_wos_field(grid, mean_in,
   % covariance_in, grid, no_samples, method)
   %
   % Driver routine for Walk-on-spheres algorithm
   % Uses WoS random walks to update current Gaussian posterior,
   % based on no_samples (=number of Wos samples)
   %
   % Inputs
   % grid:
   % params.alpha: stability index (Laplace exponent is params.alpha/2)
   % params.rhs: source term (right-hand side) function
   % params.ext: exterior term function
   % params.d: distance to the domain-boundary function
   %
   % method: method parameter (0=no rhs; 1=Monte Carlo; 2=trapezioum)
   % no_samples: number of samples per batch
   % method_params: number of samples of (R, Theta) (method=1) or
   % [number of samples of R, number of samples of Theta] (method=2)
   %
   if (method==1)
       no_samp_r = method_params(1);
       no_samp_theta = no_samp_r;
   elseif (method==2)
       no_samp_r = method_params(1);
       no_samp_theta = method_params(2);
   else
       no_samp_r = 0;
       no_samp_theta = 0;
   end
% extract diagonal of current covariance and find max
D=diag(covariance_in);

[max_var,ind]=max(D(:));


sample_var=grid.data.C(ind);


if (length(ind)==0)
  exit(1)
end
% add new samples to ind
% loop grid points, generate sample mean and variance
data=zeros(length(ind),1); data_var=data;
for i=1:length(ind),
  j=ind(i);
  x0=grid.i(:,j);
  if grid.data.no(ind)==0
    no_per_batch=no_samples;
  else
    no_per_batch=ceil(10*sample_var(i)/max_var(i));
  end
  [valf, valg, sum_balls_batch, no_balls_batch]=...
  fn_wos_core_estimation(x0, params,...
      no_per_batch, method, no_samp_r, no_samp_theta);
  grid.data.no(j)=grid.data.no(j)+no_per_batch;
  %
  lambda=no_per_batch/(grid.data.no(j));
  grid.data.mu(j)=(1-lambda)*grid.data.mu(j) +(lambda)*mean(valf+valg);
  grid.data.C(j) =(1-lambda)^2*grid.data.C(j) +(lambda)^2*var(valf+valg);
end
%
ind=find(grid.data.no(:)>0);
data=grid.data.mu(ind); data_var=grid.data.C(ind);

[mean_out,covariance_out]=new_data(mean_prior,cov_prior,data,data_var,ind);
end
