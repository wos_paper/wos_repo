function [ out ] = g4( x, alpha )
% [out]=g4(x,alpha)
%
% Source-term function: Non-constant example
% From Dyda, page 549, Table 3, Example 2
% Exact solution for this problem with exterior=0 is u4
%
% Inputs
% x: point(s) in R^n for evaluation
% alpha: stability index (Laplace exponent is params.alpha/2)
const=(2^alpha)*gamma(2+(alpha/2))*gamma(1+(alpha/2));
out =const*(1-((1+(alpha/2))*sum(x.^2)));
end
