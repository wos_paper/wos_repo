alpha=0.1;
epsilon=1e-3;
Npoints=1e5;
addpath('..')
%
axis_font_size=8; height=6; marker_size=3;
mycolor=0.9*[1,1,0];
myalpha=0.4;
%
alpha=0.9;
Npoints=1e6;
epsilon=1e-6;
marker_size=3;
z=sample_path([0;0],alpha,Npoints,epsilon);
plot(z(:,1),z(:,2),'.','MarkerSize',marker_size)
hold on;
s=plot(z(:,1),z(:,2),'-','Color',mycolor)
s.Color(4)=myalpha
xlabel('$x$','interpreter','latex')
ylabel('$y$','interpreter','latex')
set(gca,'FontSize',axis_font_size)
filename='test5_alpha0_9.pdf'
adjustpdfpage(gcf,height) %cm height
print(filename,'-dpdf','-r300')
hold off;
%
alpha=1.8;
Npoints=1e5;
epsilon=1e-3;
myalpha=0.3;
marker_size=3;
z=sample_path([0;0],alpha,Npoints,epsilon);
plot(z(:,1),z(:,2),'.','MarkerSize',marker_size)
hold on;
s=plot(z(:,1),z(:,2),'-','Color',mycolor)
s.Color(4)=myalpha
xlabel('$x$','interpreter','latex')
ylabel('$y$','interpreter','latex')
set(gca,'FontSize',axis_font_size)
filename='test5_alpha1_8.pdf'
adjustpdfpage(gcf,height) %cm height
print(filename,'-dpdf','-r300')
hold off;
