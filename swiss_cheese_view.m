clear all
N=10;
R=N+3; h=0.05;
g=@(x) d_swiss_cheese(x,N)
[X,Y]=meshgrid(-R:h:R,-R:h:R);

Z=X;
for i=1:length(X);
    for j=1:length(Y);
        Z(i,j)=g([X(i,j),Y(i,j)]);
    end;
end

height=7.5 ; % height of pdf figure in cm
axis_font_size=7
figure(1)
%contourf(X,Y,sign(Z));
contour(X,Y,sign(Z)); % gives coloful contour lines
%set(gcf, 'colormap', gray) % gives gray scale filled contour plot
ylabel('$x_2$','interpreter','latex') % y-axis label
xlabel('$x_1$','interpreter','latex') % x-axis label
set(gca,'FontSize',axis_font_size)
adjustpdfpage(gcf,height) %cm height
filename='swiss_cheese_domain'
print(filename,'-dpdf','-r300')

