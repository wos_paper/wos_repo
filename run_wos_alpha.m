% This script runs WoS for different alpha and plots results

clear all
start_dt = now; fprintf('Start time: %s\n',datestr(start_dt));
save_results = true;
write = true;
figures = true;
exact_given = false;

%%%%%%%%%%%%%%%%%%%%% Set starting point and parameters %%%%%%%%%%%%%%%%%%%
x0 = [sqrt(0.29);-sqrt(0.7)];%[0.6;0.6];
alpha_vec=0.1:0.1:1.9;

test='test4';
params.ext=@f0;
params.rhs=@g4;
params.d=@d_ball_0_1;
exact=@u4;
tolerance=1e-6;
max_samples=1e3;
no_per_batch=400;
method=1;
method_params=1e3;%[200,100];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Initialise %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
est = zeros(1,length(alpha_vec));
steps = zeros(1,length(alpha_vec));
times = ones(1,length(alpha_vec));
samplevar = zeros(1,length(alpha_vec));
estvar = zeros(1,length(alpha_vec));
est_sd = zeros(1,length(alpha_vec));
actual = zeros(1,length(alpha_vec));
abserrors = zeros(1,length(alpha_vec));
relerrors = zeros(1,length(alpha_vec));
no_samples_vec = zeros(1,length(alpha_vec));
work = zeros(1,length(alpha_vec));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Run WoS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for j=1:length(alpha_vec)
    tic
    params.alpha=alpha_vec(j);
    if (exact_given)
        [estimate, sample_var, mean_no_steps, err_est, no_samples] = fn_run_wos(x0, ...
            params, tolerance, method, max_samples, no_per_batch, method_params, exact);
    else
        [estimate, sample_var, mean_no_steps, err_est, no_samples] = fn_run_wos(x0, ...
            params, tolerance, method, max_samples, no_per_batch, method_params);
    end
    %
    times(j) = toc
    est(j) = sum(estimate)
    steps(j) = mean_no_steps
    samplevar(j) = sum(sample_var)
    no_samples_vec(j) = no_samples(2)
    est_sd(j) = sqrt(samplevar(j)/no_samples(2)); % standard deviation of estimate
    work(j)= no_samples(2)*mean_no_steps;
    if (exact_given)
        actual(j) = exact(x0,alpha_vec(j))
        abserrors(j) = abs(est(j)-actual(j)); % abs error
        relerrors(j) = abs(actual(j)-est(j))./actual(j); % rel err
    end
end

%%%%%%%%%%%%%%%%%%%% Save workspace and write to file %%%%%%%%%%%%%%%%%%%%%
end_dt=now;
file_dt = datestr(end_dt,'dd-mm-yy_HH.MM');
if (save_results)
    save(strcat(test,'_workspace_met',num2str(method),'_',file_dt,'.mat'))
end
if (write)
    fileID = fopen(strcat(test,'_parameters_met',num2str(method),'_',file_dt),'w+');
    fprintf(fileID,'Program start time: %s\n',datestr(start_dt));
    fprintf(fileID,'Computation end time: %s\n',datestr(end_dt));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Figures %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (figures)
    height=7.5 ; % height of pdf figure in cm
    axis_font_size=7

    figure(1)
    plot(alpha_vec, steps, 'Marker', 'o');
    ylabel('Average number of steps') % y-axis label
    xlabel('$\alpha$','interpreter','latex') % x-axis label
    set(gca,'FontSize',axis_font_size)
    adjustpdfpage(gcf,height) %cm height
    filename=strcat(test,'_fig1_met',num2str(method))
    print(filename,'-dpdf','-r300')
    title(strcat('E[N] vs Alpha for x_0=(',num2str(x0(1)),',',num2str(x0(2)),')'))

    if (exact_given)
        figure(2)
        semilogy(alpha_vec, abserrors, 'Marker', 'o');
        ylabel('Absolute error') % y-axis label
        xlabel('$\alpha$','interpreter','latex') % x-axis label
        set(gca,'FontSize',axis_font_size)
        adjustpdfpage(gcf,height) %cm height
        filename=strcat(test,'_fig2_met',num2str(method))
        print(filename,'-dpdf','-r300')
        title(strcat('Test ',num2str(test),': Absolute error of u_',num2str(test),...
            '(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))
    end
    
    figure(3)
    plot(alpha_vec, est_sd, 'Marker', 'o');
    ylabel('Estimator standard deviation') % y-axis label
    xlabel('$\alpha$','interpreter','latex') % x-axis label
    set(gca,'FontSize',axis_font_size)
    adjustpdfpage(gcf,height) %cm height
    filename=strcat(test,'_fig3_met',num2str(method))
    print(filename,'-dpdf','-r300')
    title(strcat('Test ',num2str(test),': Estimator standard deviation of u_',num2str(test),...
        '(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))

    figure(4)
    semilogy(alpha_vec, samplevar, 'Marker', 'o');
    ylabel('Sample variance') % y-axis label
    xlabel('$\alpha$','interpreter','latex') % x-axis label
    set(gca,'FontSize',axis_font_size)
    filename=strcat(test,'_fig4_met',num2str(method))
    adjustpdfpage(gcf,height) %cm height
    print(filename,'-dpdf','-r300')
    title(strcat('Test ',num2str(test),': Sample variance of u_',num2str(test),...
        '(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))

    if (exact_given)
        figure(5)
        semilogy(alpha_vec, relerrors, 'Marker', 'o');
        ylabel('Relative error') % y-axis label
        xlabel('$\alpha$','interpreter','latex') % x-axis label
        set(gca,'FontSize',axis_font_size)
        adjustpdfpage(gcf,height) %cm height
        filename=strcat(test,'_fig5_met',num2str(method))
        print(filename,'-dpdf','-r300')
        title(strcat('Test ',num2str(test),': Relative error of u_',num2str(test),...
            '(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))
    end
    
    figure(6)
    semilogy(alpha_vec, no_samples_vec, 'Marker', 'o');
    ylabel('Number of samples') % y-axis label
    xlabel('$\alpha$','interpreter','latex') % x-axis label
    set(gca,'FontSize',axis_font_size)
    filename=strcat(test,'_fig6_met',num2str(method))
    adjustpdfpage(gcf,height) %cm height
    print(filename,'-dpdf','-r300')
    title(strcat('Test ',num2str(test),': Number of samples of u_',num2str(test),...
        '(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))

    figure(7)
    semilogy(alpha_vec, work, 'Marker', 'o');
    ylabel('Number of samples and steps') % y-axis label
    xlabel('$\alpha$','interpreter','latex') % x-axis label
    set(gca,'FontSize',axis_font_size)
    filename=strcat(test,'_fig7_met',num2str(method))
    adjustpdfpage(gcf,height) %cm height
    print(filename,'-dpdf','-r300')
    title(strcat('Test ',num2str(test),': Number of samples and steps (work) of u_',num2str(test),...
        '(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))
end
%%%%%%%%%%%%%%%%%%%%%%%% Write parameters to file %%%%%%%%%%%%%%%%%%%%%%%%%
if (write)
    fprintf(fileID,'Program end time: %s\n',datestr(now));
    fprintf(fileID,'Test ID: %s\n',test); fprintf(fileID,'\n');
    fprintf(fileID,'x0 = [%8.3f;%8.3f]\n',x0(1),x0(2));
    fprintf(fileID,'params.g = @%s\n',func2str(params.g));
    fprintf(fileID,'params.f = @%s\n',func2str(params.f));
    fprintf(fileID,'params.d = @%s\n',func2str(params.d));
    fprintf(fileID,'exact = @%s\n',func2str(exact));
    fprintf(fileID,'tolerance = %2.0e\n',tolerance);
    fprintf(fileID,'max_samples = %2.0e\n',max_samples);
    fprintf(fileID,'no_per_batch = %8.0f\n',no_per_batch);
    fprintf(fileID,'method = %d\n',method);
    if (method==1)
        fprintf(fileID,'method_params = %8.0f',method_params(1));
    elseif (method==2)
        fprintf(fileID,'method_params = [%8.0f;%8.0f]',method_params(1),...
            method_params(2));
    end
    fclose(fileID);
end