function out=swiss_cheese_vectorised(x,N)
% vectorised swiss_chesse
% columns of the matrix x contains 2-vectors
[n,m]=size(x);
out=zeros(1,m);
for i=1:m,
  out(i)=d_swiss_cheese(x(:,i),N);
end
return
