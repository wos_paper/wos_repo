% This script runs WoS for different no_samp_theta and plots results
clear all
start_dt = now; fprintf('Start time: %s\n',datestr(start_dt));
write = false;
figures = false;

%%%%%%%%%%%%%%%%%%%%% Set starting point and parameters %%%%%%%%%%%%%%%%%%%
x0 = [0.6;0.6];
no_theta_vec=[10,20,50,70,100,150];%,130,160,200,230,260,300];

test='test4';
params.alpha=1;
params.rhs=@g4;
params.ext=@f0;
params.d=@d_ball_0_1;
exact=@u4;
tolerance=5*1e-3;
max_samples=1e6;
no_per_batch=300;
method=2;
no_samp_r=200;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Initialise %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n = length(no_theta_vec);
est = zeros(1,n);
steps = zeros(1,n);
times = ones(1,n);
samplevar = zeros(1,n);
estvar = zeros(1,n);
est_sd = zeros(1,n);
samplevar1 = zeros(1,n);
estvar1 = zeros(1,n);
actual = zeros(1,n);
abserrors = zeros(1,n);
relerrors = zeros(1,n);
no_samples_vec = zeros(1,n);
work = zeros(1,n);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Run WoS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for j=1:n
    tic
    method_params=[no_samp_r,no_theta_vec(j)]
    [estimate, sample_var, mean_no_steps,error,no_samples]=fn_run_wos(x0, ...
        params, tolerance, method, max_samples, no_per_batch, method_params, exact);
    %
    times(j) = toc
    est(j) = sum(estimate)
    steps(j) = mean_no_steps
    samplevar(j) = sum(sample_var)
    no_samples_vec(j)=no_samples(2)
    est_sd(j)= sqrt(samplevar(j)/no_samples(2)); % standard deviation of estimate
    work(j)=no_samples(2)*mean_no_steps;
    actual(j) = exact(x0,params.alpha)
    abserrors(j) = abs(est(j)-actual(j)); % abs error
    relerrors(j) = abs(actual(j)-est(j))./actual(j); % rel err
end
%%%%%%%%%%%%%%%%%%%%%%%% Write parameters to file %%%%%%%%%%%%%%%%%%%%%%%%%
if (write)
    end_dt=now;
    file_dt = datestr(end_dt,'dd-mm-yy_HH.MM');
    fileID = fopen(strcat(test,'_parameters_',file_dt),'w+');
    fprintf(fileID,'Program start time: %s\n',datestr(start_dt));
    fprintf(fileID,'Computation end time: %s\n',datestr(end_dt));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Figures %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (figures)
    height=7.5 ; % height of pdf figure in cm
    axis_font_size=7

    figure(1)
    plot(alpha_vec, steps, 'Marker', 'o');
    %hold on
    %plot(Avec,p.^(-1),)
    ylabel('Average number of steps') % x-axis label
    %xlabel('Alpha') % y-axis label
    xlabel('$\alpha$','interpreter','latex')
    set(gca,'FontSize',axis_font_size)
    adjustpdfpage(gcf,height) %cm height
    filename=strcat(test,'_fig1_met',num2str(method))
    print(filename,'-dpdf','-r300')
    title(strcat('E[N] vs Alpha for x_0=(',num2str(x0(1)),',',num2str(x0(2)),')'))
    %hold off
    % p = polyfit(Avec,steps,-2)
    % f = polyval(p,Avec);
    % hold on
    % plot(Avec,f,'--r')
    % hold off
    % cftool

    figure(2)
    semilogy(alpha_vec, abserrors, 'Marker', 'o');
    ylabel('Absolute error') % x-axis label
    %xlabel('Alpha') % y-axis label
    xlabel('$\alpha$','interpreter','latex')
    set(gca,'FontSize',axis_font_size)
    adjustpdfpage(gcf,height) %cm height
    filename=strcat(test,'_fig2_met',num2str(method))
    print(filename,'-dpdf','-r300')
    title(strcat('Test ',num2str(test),': Absolute error of u_',num2str(test),...
        '(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))

    figure(3)
    plot(alpha_vec, est_sd, 'Marker', 'o');
    ylabel('Estimator standard deviation') % x-axis label
    %xlabel('Alpha') % y-axis label
    xlabel('$\alpha$','interpreter','latex')
    set(gca,'FontSize',axis_font_size)
    adjustpdfpage(gcf,height) %cm height
    filename=strcat(test,'_fig3_met',num2str(method))
    print(filename,'-dpdf','-r300')
    title(strcat('Test ',num2str(test),': Estimator variance of u_',num2str(test),...
        '(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))

    figure(4)
    semilogy(alpha_vec, samplevar, 'Marker', 'o');
    ylabel('Sample variance') % x-axis label
    %xlabel('Alpha') % y-axis label
    xlabel('$\alpha$','interpreter','latex')
    set(gca,'FontSize',axis_font_size)
    filename=strcat(test,'_fig4_met',num2str(method))
    adjustpdfpage(gcf,height) %cm height
    print(filename,'-dpdf','-r300')
    title(strcat('Test ',num2str(test),': Sample variance of u_',num2str(test),...
        '(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))

    figure(5)
    semilogy(alpha_vec, relerrors, 'Marker', 'o');
    ylabel('Relative error') % x-axis label
    %xlabel('Alpha') % y-axis label
    xlabel('$\alpha$','interpreter','latex')
    set(gca,'FontSize',axis_font_size)
    adjustpdfpage(gcf,height) %cm height
    filename=strcat(test,'_fig5_met',num2str(method))
    print(filename,'-dpdf','-r300')
    title(strcat('Test ',num2str(test),': Absolute error of u_',num2str(test),...
        '(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))

    figure(6)
    semilogy(alpha_vec, no_samples_vec, 'Marker', 'o');
    ylabel('Number of samples') % x-axis label
    %xlabel('Alpha') % y-axis label
    xlabel('$\alpha$','interpreter','latex')
    set(gca,'FontSize',axis_font_size)
    filename=strcat(test,'_fig6_met',num2str(method))
    adjustpdfpage(gcf,height) %cm height
    print(filename,'-dpdf','-r300')
    title(strcat('Test ',num2str(test),': Number of samples of u_',num2str(test),...
        '(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))

    figure(7)
    semilogy(alpha_vec, work, 'Marker', 'o');
    ylabel('Number of samples and steps') % x-axis label
    %xlabel('Alpha') % y-axis label
    xlabel('$\alpha$','interpreter','latex')
    set(gca,'FontSize',axis_font_size)
    filename=strcat(test,'_fig7_met',num2str(method))
    adjustpdfpage(gcf,height) %cm height
    print(filename,'-dpdf','-r300')
    title(strcat('Test ',num2str(test),': Number of samples of u_',num2str(test),...
        '(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))
end
%%%%%%%%%%%%%%%%%%%%%%%% Write parameters to file %%%%%%%%%%%%%%%%%%%%%%%%%
if (write)
    fprintf(fileID,'Program end time: %s\n',datestr(now));
    fprintf(fileID,'Test ID: %s\n',test); fprintf(fileID,'\n');
    fprintf(fileID,'x0 = [%8.3f;%8.3f]\n',x0(1),x0(2));
    fprintf(fileID,'params.g = @%s\n',func2str(params.g));
    fprintf(fileID,'params.f = @%s\n',func2str(params.f));
    fprintf(fileID,'params.d = @%s\n',func2str(params.d));
    fprintf(fileID,'exact = @%s\n',func2str(exact));
    fprintf(fileID,'tolerance = %2.0e\n',tolerance);
    fprintf(fileID,'max_samples = %2.0e\n',max_samples);
    fprintf(fileID,'no_per_batch = %8.0f\n',no_per_batch);
    fprintf(fileID,'method = %d\n',method);
    if (method==1)
        fprintf(fileID,'method_params = %8.0f',method_params(1));
    elseif (method==2)
        fprintf(fileID,'method_params = [%8.0f;%8.0f]',method_params(1),...
            method_params(2));
    end
    fclose(fileID);
end