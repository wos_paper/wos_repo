function [ out ] = g3( x, alpha )
% [out]=g3(x,alpha)
%
% Source-term function: Constant example
% From Dyda, page 549, Table 3, Example 2
% Exact solution for this problem with exterior=0 is u3
%
% Inputs
% x: point(s) in R^n for evaluation
% alpha: stability index (Laplace exponent is params.alpha/2)

[~,m]=size(x);
out = (2^alpha)*gamma(1+(alpha/2))*(alpha/2)*beta(alpha/2,1-(alpha/2))/...
    gamma(1-(alpha/2))*ones(1,m);

end
