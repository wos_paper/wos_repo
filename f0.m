function [ out ] = f0( x, alpha )
% [out]=f0(x,alpha)
%
% Zero exterior function (vectorised)
%
% Inputs
% x: point(s) in R^n for evaluation
% alpha: stability index (Laplace exponent is params.alpha/2)

[~,m]=size(x);
out = zeros(1,m);

end

