function [val_hom, val_inh, j_sum, j] = fn_wos_core_estimation( x0, params, no_samples, method, no_samp_r, no_samp_theta)
% [val_hom, val_inh, j_sum, j] = fn_wos_core_estimation( x0, params, no_samples, method, no_samp_r, no_samp_theta)
%
% Executes WoS random walks no_samples-times and outputs estimates and N
%
% Input
% x0: initial position
% params.alpha: stability index (Laplace exponent is params.alpha/2)
% params.rhs: source term (right-hand side) function
% params.ext: exterior term function
% params.d: distance to the domain boundary function
% no_samples: number of samples to perform
% method: method of approximating the inner integral over theta (1 = Monte Carlo, 2 = trapezoidal rule)
% no_samp_r: number of samples of the radius for Monte Carlo over r (and theta for method 1)
% no_samp_theta: number of samples of theta for trapezoidal rule
%
% Output
% val_hom: vector of f(y) where y is exit point (homogeneous part of the solution)
% val_inh: vector of estimates of the inhomogeneous part of the solution
% j_sum: sum of balls for all WoS runs
% j: vector of the number of balls for each WoS run
%
coder.inline('always');
%
alpha = params.alpha;

% Initial distance to boundary
d0=params.d(x0);
%
% Initialise
j=zeros(no_samples,1);
val_hom=zeros(no_samples,1);
val_inh = zeros(no_samples,1);
%
const = (alpha^(-1))*(2^(-alpha+1))*(gamma(alpha/2)^(-2))*beta((2-alpha)/2,alpha/2);
z=(2-alpha)/2; w=alpha/2;

if (method>0) % Use when rhs non-zero and method set to 1 or 2
    val_inh = zeros(no_samples,1);
    if (method==1) % Monte Carlo method
        for i=1:no_samples
            % Initial position of WoS
            d=d0;   y=x0;
            % Walk on spheres
            while (d>0)
              % Calculate the inhomogeneous part for current sphere (y,d)
              no_samples_scaled=ceil(no_samp_r*d^2);
              r_samples = (rand([1,no_samples_scaled]).^(1/alpha));
              theta_samples = 2*pi*rand([1,no_samples_scaled]);
              zz=eval_MC(r_samples,theta_samples,y,d,alpha,const, params.value,params.rhs);
              val_rho_inh =mean(zz);
              val_inh(i) = val_inh(i)+val_rho_inh;
              % Next sphere (y,d)
              r1 = cdfinv(rand,alpha,d); theta1 = rand*2*pi;
              y = y + r1*[cos(theta1); sin(theta1)];
              d=params.d(y);
              j(i)=j(i)+1;
            end % Exit domain
            % Calculate homogeneous part at the exit point
            val_hom(i)=params.ext(y,alpha);
        end % End i loop
    else % Trapezoidal rule method
        h = 2*pi/no_samp_theta;
        theta_samples = h*(0:no_samp_theta-1);
        for i=1:no_samples
            % Sample r for each run
            r_samples = (rand([1,no_samp_r]).^(1/alpha));
            % Initial position of WoS
            d=d0;   y=x0;
            % Walk on spheres
            while (d>0)
              % Calculate the inhomogeneous part for current sphere (y,d)
              val_rho_inh = mean(trap_rule(theta_samples,h,r_samples,y,d,...
                  alpha,params.rhs).*eval_part_TR(r_samples,alpha,const));
              val_inh(i) = val_inh(i)+val_rho_inh;
              % Next sphere (y,d)
              r1 = cdfinv(rand,alpha,d);theta1 = rand*2*pi;
              y = y + [r1*cos(theta1); r1*sin(theta1)];
              d=params.d(y);
              j(i)=j(i)+1;
            end % Exit domain
            % Calculate the homogeneous part at the exit point
            val_hom(i)=params.ext(y,alpha);
        end % End i loop
    end
else % Use when rhs is zero and method set to 0
    for i=1:no_samples
        d=d0;
        y=x0;
        while (d>0)
          r1 = cdfinv(rand,alpha,d);
          theta1 = rand*2*pi;
          y = y + [r1*cos(theta1); r1*sin(theta1)];
          d=params.d(y);
          j(i)=j(i)+1;
        end % Exit domain
        % Calculate the homogeneous part at the exit point
        val_hom(i)=params.ext(y,alpha);
    end % End i loop
end

j_sum=sum(j);

end
