function [ out ] = d_two_balls( x )
% [out]=d_two_balls(x)
%
% Calculates the distance of points inside the domain to the domain boundary
% Note: This function is vectorised
% Domain is consists of two touching balls
% Ball 1: radius R1=1, centred at O1=[0;...;0] in R^n
% Ball 2: radius R2=0.5, centred at O2=[0;...;R1+R2] in R^n
%
% Inputs
% x: point(s) in R^n for which distance is computed

R1=1;
R2=0.5;
[n,m]=size(x);
O1=zeros(n,m);
O2=[zeros(n-1,m);(R1+R2)*ones(1,m)];
dist = [R1-sqrt(sum((x-O1).^2));R2-sqrt(sum((x-O2).^2))];
idx = (sum(dist>0)==2);
out = zeros(1,n);
if (sum(idx)>0)
    out(idx) = min(dist(:,idx));
end
if (sum(~idx)>0)
    out(~idx) = max(dist(:,~idx));
end

end

