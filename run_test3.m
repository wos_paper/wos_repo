params.alpha=1;  tol=1e-2; method=1;
x0=[0.5;0.5]
params.f = @f0;
params.g = @g3;
params.d = @d_ball_0_1;
exact = @u3;
U=[0;0;1]; % initial position and radius
fprintf('Circle domain\nRadius: %8.2f\n',U(3))
fprintf('Centre: (%8.2f, %8.2f    )\n',U(1),U(2))
fprintf('Initial position: (%8.2f, %8.2f    )\n',x0(1), x0(2))
% [mean_val, var_sample, mean_balls, err_est, no_samples] = fn_run_wos(x0, params, tol, method, 1e4, 400, 1e3)
% u5=sum(mean_val)
% u5e=u2(x0,alpha)
% ratio5=u5e/u5
% %%%%%%%%%
x0=[sqrt(0.29);sqrt(0.7)]
% [mean_val, var_sample, mean_balls, err_est, no_samples] = fn_run_wos(x0, params, tol, method, 1e4, 400, 1e3)
% u3=sum(mean_val)
% u3e=u2(x0,alpha)
% ratio3=u3e/u3
%
%x0=[0;0]
[mean_val, var_sample, mean_balls, err_est, no_samples] = fn_run_wos(x0, params, tol, method, max_samples, 400, 1e3, exact)

% u0=sum(mean_val)
% u0e=u2(x0,alpha)
% ratio0=u0e/u0
% disp('-------------------')
% [u5,u5e,ratio5]
% [u3,u3e,ratio3]
% [u0,u0e,ratio0]
% test exact soln
%x=rand; alp=2;[hypergeom([1,-alp/2],1,norm(x)^2), (1-norm(x)^2)^(alp/2)]
