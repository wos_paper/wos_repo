clear all
tic
start_dt = now; fprintf('Start time: %s\n',datestr(start_dt));
x0=[sqrt(0.29);sqrt(0.7)];%x0=[0.6;0.6];
fprintf('x0 = [%8.3f;%8.3f]\n',x0(1),x0(2));
params.alpha=1; fprintf('alpha = %1.1f\n',params.alpha)
test='test5'; fprintf('Test ID: %s\n',test);
params.ext = @f1;
params.rhs = @g0;
params.d = @d_ball_0_1;
exact = @f1;
tol=1e-6;
max_samples=1e4;
no_per_batch=100;
method=0;
method_params=1000; %[200,200];

%x0=[0.5;0.5]
%x0=[sqrt(0.29);sqrt(0.7)]
%x0=[0.6;0.6]
%x0=[0.001;0.001]
[mean_val, var_sample, mean_balls, err_est, no_samples, no_balls] = fn_run_wos_hist(x0, params, tol, method, max_samples, no_per_batch, method_params)

load p_est.mat
p=est(params.alpha*10);
geom=p*(1-p).^(0:(max(no_balls)-1));
pn = zeros(1,max(no_balls)+1);
for i=0:max(no_balls)
    pn(i+1) = sum(no_balls>i)/length(no_balls);
end

height=7.5 ; % height of pdf figure in cm
axis_font_size=7
figure(1)
histogram(no_balls,[min(no_balls):max(no_balls)]-0.5,'Normalization','probability')%,'BinWidth',1.0)
hold on
plot(1:max(no_balls),geom,'r.-')
ylabel('Probability') % x-axis label
%   %xlabel('Alpha') % y-axis label
xlabel('Number of steps')%,'interpreter','latex')
legend('Histogram of number of steps','pdf of Geom(p)')
hold off
set(gca,'FontSize',axis_font_size)
adjustpdfpage(gcf,height) %cm height
filename=strcat(test,'_hist1')
print(filename,'-dpdf','-r300')
%title('Histogram of the number of steps for 10^6 samples')

figure(2)
bar(0:max(no_balls),pn,1,'w')%plot(0:max(no_balls),pn)%
hold on
plot(0:max(no_balls),(1-p).^(0:max(no_balls)),'r.-')
ylabel('Probability') % x-axis label
%   %xlabel('Alpha') % y-axis label
xlabel('n')%,'interpreter','latex')
legend('Proportion of samples with N_x>n','(1-p)^n')
axis([-0.5, max(no_balls)+0.5, 0, 1.05])
hold off
set(gca,'FontSize',axis_font_size)
adjustpdfpage(gcf,height) %cm height
filename=strcat(test,'_hist2')
print(filename,'-dpdf','-r300')
%title('Histogram of the number of steps for 10^6 samples')
