function [ out ] = rhs_polar_TR( r, theta, rhon, rn, alpha, rhs_fn )
% Ana: vectorised in every possible combination of r and theta
% Output columns are values for a U(0,pi) shifted mesh for theta and for a fixed r

m = length(theta);
n = length(r);

% phase-shift to remove truncation bias
shift = reshape(repmat(rand(1,n)*pi,m,1),[1,n*m]);
theta1 = repmat(theta,1,n)+shift;
r1 = reshape(repmat(r,m,1),[1,n*m]);

points = [cos(theta1).*r1;sin(theta1).*r1];
%[reshape(transpose(cos(theta))*r,[1,n*m]);...
%    reshape(transpose(sin(theta))*r,[1,n*m])];

out = reshape(rhs_fn(repmat(rhon,1,n*m)+rn*points,alpha),[m,n]);

end
