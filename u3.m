function [ out ] = u3( x, alpha )
% [out]=u3(x,alpha,d)
%
% Model solution: Constant source example
% From Dyda, page 549, Table 3, Example 1
% Note: This function is vectorised
%
% Inputs
% x: point(s) in R^n for evaluation
% alpha: stability index (Laplace exponent is params.alpha/2)

[~,m]=size(x);
r=d_ball_0_1(x);
idx=(r>0);
m1=sum(idx);
out=zeros(1,m);
if (m1>0)
    out(idx) = max([0*ones(1,m1);(1*ones(1,m1)-sum(x(:,idx).^2)).^(alpha/2)]);
end
if (m-m1>0)
    out(~idx) = zeros(1,m-m1);
end

end
