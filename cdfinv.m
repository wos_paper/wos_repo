function out= cdfinv(y,alpha,r)
% out = cdfinv(y,alpha,r)
%
% Inverse of CDF of exit distribution from ball
%
% Input
% y: U(0,1) RV
% alpha: stability index of Levy process
% r: radius of ball (distance to boundary)
%
coder.inline('always');
% Note: funcdf(r,alpha,r)=-1
% Note: sin(pi*alpha/2) * beta(alpha/2, 1-(alpha/2) ) =pi
% tmp1=real(pi*y...
% / ( sin(pi*alpha/2)   *  beta(alpha/2, 1-(alpha/2) ) ) ;
% tmp=y
tmp = betaincinv(y, alpha/2, 1-(alpha/2));
out = r/sqrt(real(tmp));
end
