function [ out ] = rhs_polar2( r,theta,rhon,rn,alpha,rhs_fn )
% [out]=rhs_polar(r,theta,rhon,rn,alpha,rhs_fn)
%
% Function rhs_fn(rhon+rn*x) in terms of polar coordinates of x
% Vectorised in r and theta, such that r and theta must have the same length
% or one of them must be a scalar

%m=max([length(r),length(theta)]);
 % out = rhs_fn(repmat(rhon,1,m)+rn*[r.*cos(theta);r.*sin(theta)],alpha)-rhs_fn(rhon,alpha);
 Rscaled=rn*r;
 out = rhs_fn([rhon(1)+Rscaled.*cos(theta);rhon(2)+Rscaled.*sin(theta)],alpha)-rhs_fn(rhon,alpha);
end
