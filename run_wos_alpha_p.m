x0 = [0;0];
Avec=0.1:0.1:1.9;

test='p';
params.rhs=@f0;
params.ext=@f_half_plane;
params.d=@d_ball_0_1;
%exact=@u2;
method=0;
tol=1e-4;
max_samples=1e4;

xvec=0;
cdfvec=0;
est = zeros(1,length(Avec));
steps = zeros(1,length(Avec));
times = ones(1,length(Avec));
samplevar = zeros(1,length(Avec));
estvar = zeros(1,length(Avec));
est_sd = zeros(1,length(Avec));
samplevar1 = zeros(1,length(Avec));
estvar1 = zeros(1,length(Avec));
%actual = zeros(1,length(Avec));
%abserrors = zeros(1,length(Avec));
%relerrors = zeros(1,length(Avec));

no_samples_vec = zeros(1,length(Avec));

work = zeros(1,length(Avec));

for j=1:length(Avec)
    tic
    params.alpha=Avec(j);
    [estimate, sample_var, mean_no_steps,error,no_samples]=fn_run_wos(x0, params, tol, method, max_samples, 400, 1e3);
    %
    times(j) = toc
    est(j) = sum(estimate)
    steps(j) = mean_no_steps
    samplevar(j) = sum(sample_var)
    no_samples_vec(j)=no_samples(2)
    est_sd(j)= sqrt(samplevar(j)/no_samples(2)); % standard deviation of estimate
  %samplevar1(j) = var(mcvec(1:(Nvec/4)));
  %  estvar1(j)= sqrt(samplevar(j)/(Nvec/4));
    work(j)=no_samples(2)*mean_no_steps;
%    actual(j) = exact(x0,Avec(j))
%    abserrors(j) = abs(est(j)-actual(j)); % abs error
%    relerrors(j) = abs(actual(j)-est(j))./actual(j); % rel err
end

save('p.mat','est','Avec')

%%% FIGURES %%%
%   height=7.5 ; % height of pdf figure in cm
%   axis_font_size=7
%   figure(1)
%   plot(Avec, steps, 'Marker', 'o');
%   ylabel('Average number of steps') % x-axis label
%   %xlabel('Alpha') % y-axis label
%   xlabel('$\alpha$','interpreter','latex')
%   set(gca,'FontSize',axis_font_size)
%   adjustpdfpage(gcf,height) %cm height
%   filename=strcat(test,'_fig1')
%   print(filename,'-dpdf','-r300')
%   title(strcat('E[N] vs Alpha for x_0=(',num2str(x0(1)),',',num2str(x0(2)),')'))
%   % p = polyfit(Avec,steps,-2)
%   % f = polyval(p,Avec);
%   % hold on
%   % plot(Avec,f,'--r')
%   % hold off
%   % cftool
% 
% 
%     figure(2)
%     semilogy(Avec, abserrors, 'Marker', 'o');
%     ylabel('Absolute error') % x-axis label
%     %xlabel('Alpha') % y-axis label
%     xlabel('$\alpha$','interpreter','latex')
%     set(gca,'FontSize',axis_font_size)
%     adjustpdfpage(gcf,height) %cm height
%     filename=strcat(test,'_fig2')
%     print(filename,'-dpdf','-r300')
%     title(strcat('Test ',num2str(test),': Absolute error of u_',num2str(test),'(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))
% 
% 
% 
%   figure(3)
%   plot(Avec, est_sd, 'Marker', 'o');
%   ylabel('Estimator standard deviation') % x-axis label
%   %xlabel('Alpha') % y-axis label
%   xlabel('$\alpha$','interpreter','latex')
%   set(gca,'FontSize',axis_font_size)
%   adjustpdfpage(gcf,height) %cm height
%   filename=strcat(test,'_fig3')
%   print(filename,'-dpdf','-r300')
%   title(strcat('Test ',num2str(test),': Estimator variance of u_',num2str(test),'(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))
% 
%   figure(4)
%   semilogy(Avec, samplevar, 'Marker', 'o');
%   ylabel('Sample variance') % x-axis label
%   %xlabel('Alpha') % y-axis label
%   xlabel('$\alpha$','interpreter','latex')
%   set(gca,'FontSize',axis_font_size)
%   filename=strcat(test,'_fig4')
%   adjustpdfpage(gcf,height) %cm height
%   print(filename,'-dpdf','-r300')
%   title(strcat('Test ',num2str(test),': Sample variance of u_',num2str(test),'(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))
% 
%   figure(5)
%   semilogy(Avec, relerrors, 'Marker', 'o');
%   ylabel('Relative error') % x-axis label
%   %xlabel('Alpha') % y-axis label
%   xlabel('$\alpha$','interpreter','latex')
%   set(gca,'FontSize',axis_font_size)
%   adjustpdfpage(gcf,height) %cm height
%   filename=strcat(test,'_fig5')
%   print(filename,'-dpdf','-r300')
%   title(strcat('Test ',num2str(test),': Absolute error of u_',num2str(test),'(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))
% 
%     figure(6)
%     semilogy(Avec, no_samples_vec, 'Marker', 'o');
%     ylabel('Number of samples') % x-axis label
%     %xlabel('Alpha') % y-axis label
%     xlabel('$\alpha$','interpreter','latex')
%     set(gca,'FontSize',axis_font_size)
%     filename=strcat(test,'_fig6')
%     adjustpdfpage(gcf,height) %cm height
%     print(filename,'-dpdf','-r300')
%     title(strcat('Test ',num2str(test),': Number of samples of u_',num2str(test),'(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))
% 
%         figure(7)
%         semilogy(Avec, work, 'Marker', 'o');
%         ylabel('Number of samples and steps') % x-axis label
%         %xlabel('Alpha') % y-axis label
%         xlabel('$\alpha$','interpreter','latex')
%         set(gca,'FontSize',axis_font_size)
%         filename=strcat(test,'_fig7')
%         adjustpdfpage(gcf,height) %cm height
%         print(filename,'-dpdf','-r300')
%         title(strcat('Test ',num2str(test),': Number of samples of u_',num2str(test),'(x) vs Alpha for x=(',num2str(x0(1)),',',num2str(x0(2)),')'))
% 
%   %tmp = zeros(1,9);
%   %for j=1:9
%   %   tmp(j) = sqrt(var(mcvec(1:(2^(j+7))))/2^(j+7));
%   %end
