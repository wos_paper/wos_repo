function [ out ] = f1( x, alpha, varargin )
% [out]=f1(x,alpha,varargin)
%
% Exterior function/model solution: Free-space Green's function centred at P
% From D1.7 of Bucur
% See Section 7, Example 1 of paper
% Note: This function is vectorised
%
% Inputs
% x: point(s) in R^n for evaluation
% alpha: stability index (Laplace exponent is params.alpha/2)

P=[2;0]; % any point outside ball

if (nargin<2)
    alpha=1;
end

[~,m]=size(x);
out=sum((x-repmat(P,[1,m])).^2).^(-1+(alpha/2));

end
