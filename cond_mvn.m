function [mu,C]=cond_mvn(mu_,C_,data,method_)
  % function [mu, C]=cond_mvn(mu_, C_, data, method=1)
  %
  % For n-dim multivariate Gaussian N(mu_,C_), let
  % X1=first k-components and
  % X2=next (n-k)-components.
  %
  % Return the mean and covariance of the
  % conditional distribution of X2 given X1=data
  % Optional argument method determines algorithm:
  % method=1: via Cholesky of full matrix (default),
  % method=2: Cholesky of data-variable sub-matrix.
  %
  % Tony Shardlow, Feb 2017.

  mu_=mu_(:);   n=length(mu_);
  data=data(:); k=length(data);
  [Cn,Cm]=size(C_); assert(n==Cn && n==Cm);
  I1=1:k; I2=k+1:n;
  switch nargin
    case 3
      method=1; % covariance of full matrix
    otherwise
      method=method_;
  end
  if method==1,
    [L,p]=chol(C_);
    assert(p==0,'C is not a covariance matrix')
    C=L(I2,I2)'*L(I2,I2);
    mu=mu_(I2)+C_(I2,I1)*(C_(I1,I1)\(data-mu_(I1)));
  else
    C11=C_(I1,I1); C12=C_(I1,I2); C22=C_(I2,I2);
    [R,p] = chol(C11);
    assert(p==0,'C11 is not a covariance matrix')
    mu    = mu_(I2) + C12' * (R \ (R'\ (data-mu_(I1))));
    C     = C22   - C12' * (R \ (R'\ C12));
  end
