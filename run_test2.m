alpha=1/2;  tol=1e-2; method=1;
x0=[0.5;0.5]
[mean_val, var_sample, mean_balls, err_est, num, numg] = fn_run_test4_adap(alpha, @g2, @f2, tol, x0, method)
u5=sum(mean_val)
u5e=u2(x0,alpha)
ratio5=u5e/u5
%%%%%%%%%
x0=[0.3;0.3]
[mean_val, var_sample, mean_balls, err_est, num, numg] = fn_run_test4_adap(alpha, @g2, @f2, tol, x0, method)
u3=sum(mean_val)
u3e=u2(x0,alpha)
ratio3=u3e/u3
%
x0=[0;0]
[mean_val, var_sample, mean_balls, err_est, num, numg] =fn_run_test4_adap(alpha, @g2, @f2, tol,x0, method)

u0=sum(mean_val)
u0e=u2(x0,alpha)
ratio0=u0e/u0
disp('-------------------')
[u5,u5e,ratio5]
[u3,u3e,ratio3]
[u0,u0e,ratio0]
% test exact soln
%x=rand; alp=2;[hypergeom([1,-alp/2],1,norm(x)^2), (1-norm(x)^2)^(alp/2)]
