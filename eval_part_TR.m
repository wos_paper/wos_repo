function [ out ] = eval_part_TR( r, alpha, const )
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

m = length(r);
out = ((2*pi)^(-1))*const*betainc(ones(1,m)-(r.^2),alpha/2,(2-alpha)/2);

end

