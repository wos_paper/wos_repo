function out=d_swiss_cheese(x,N)
% return distance to boundary of swiss cheese domain
% consisting of the interiors of circles with radius 0.5
% and centres (i,j) for (i,j) for i,j=-N,...N.
linf=norm(x,Inf);
if (linf>N+0.5)
    out=-1.0; % negative indicates exterior
    return
else
    xm=min(mod(x, 1),mod(-x,1));
    d=norm(xm,2);
    out=0.5-d; % positive indicates distance to boundary
    return
end
