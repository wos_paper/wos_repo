% This script runs WoS for one point inside the domain

tic
start_dt = now; fprintf('Start time: %s\n',datestr(start_dt));
x0=[0.6;0.6]; fprintf('x0 = [%8.3f;%8.3f]\n',x0(1),x0(2));
params.alpha=1; fprintf('alpha = %1.1f\n',params.alpha)
test='test5'; fprintf('Test ID: %s\n',test);
params.ext = @f0;
params.rhs = @g4;
params.d = @d_ball_0_1;
exact = @u4;
tol=1e-3;
max_samples=1e3;
no_per_batch=100;
method=1;
method_params=10;%[200,200];

[mean_val, var_sample, mean_balls, err_est, no_samples] = fn_run_wos(x0, params, tol, method, max_samples, no_per_batch, method_params, exact)
toc