function [ out ] = u_hom_check( x, alpha, f )
% [out]=u_hom_check(x,alpha,f,d)
%
% Solution function (vectorised): numerically computes the integral solution u(x)
% for a circle domain of radius R=1 centred at O
% See Gaussian data example, Equation (7.5) in the paper
%
% Inputs
% x: input variable to compute u(x)
% alpha: stability index (Laplace exponent is params.alpha/2)
% f: exterior term function

[~,m] = size(x);
r = d_ball_0_1(x);
idx = (r>0);
m1 = sum(idx);
out = zeros(1,m);
if (m1>0)
    idx_num = find(idx);
    for i=1:m1
        x_tmp = x(:,idx_num(i));
        fun = @(y1,y2) f([y1;y2],alpha) .*...
            (pi.^(-2)) .* sin(pi*alpha/2) .*...
            ((1-(x_tmp(1).^2+x_tmp(2).^2)).^(alpha/2)) ...
            .* ((((y1-x_tmp(1)).^2)+((y2-x_tmp(2)).^2)).^(-1));
        %polarfun = @(theta,rvar) fun(rvar.*cos(theta),rvar.*sin(theta))...
        %.*rvar.*(rvar.^2-1)^(-alpha/2);
        polarfun = @(theta,tvar) fun(sqrt(tvar.^(2/(2-alpha))+1).*cos(theta),...
        sqrt(tvar.^(2/(2-alpha))+1).*sin(theta))/(2-alpha);
        out(idx_num(i)) = integral2(polarfun,-pi,pi,0,Inf,'Method','iterated','AbsTol',1e-4,'RelTol',1e-3);
        if (isnan(out))
            out(idx_num(i)) = integral2(polarfun,-pi,pi,0,Inf,'Method','iterated','AbsTol',1e-3,'RelTol',1e-2);
            fprintf('Tolerance adjusted.\n')
            if(isnan(out))
                out(idx_num(i)) = integral2(polarfun,-pi,pi,0,Inf,'Method','iterated','AbsTol',1e-2,'RelTol',1e-2);
                fprintf('Tolerance adjusted twice.\n')
            end
        end
    end
else
    out(~idx) = f(x(~idx), alpha);
end

end
