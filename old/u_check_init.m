function [ out ] = u_check_init( x, f, alpha)
%CHECK FUNCTION computes the integral solution u(x)
% g is the boundary condition function
% alpha is stability index

if (nargin<3)
    alpha=1;
end

r = norm(x,2);
%theta = atan2(x(2),x(1));
if (r<1)
    %fprintf('r<1\n')
    fun = @(y1,y2) f([y1;y2],alpha) .*...
        (pi.^(-2)) .* sin(pi*alpha/2) .*...
        ((1-(x(1).^2+x(2).^2)).^(alpha/2)) ...
        .* (((y1.^2+y2.^2)-1).^(-alpha/2))...
        .* ((((y1-x(1)).^2)+((y2-x(2)).^2)).^(-1));
    polarfun = @(theta,rvar) fun(rvar.*cos(theta),rvar.*sin(theta)).*rvar;
       
    out = integral2(polarfun,-pi,pi,1,Inf,'Method','iterated','AbsTol',1e-4,'RelTol',1e-3);
    if (isnan(out))
        out = integral2(polarfun,-pi,pi,1,Inf,'Method','iterated','AbsTol',1e-3,'RelTol',1e-2);
        fprintf('Tolerance adjusted.\n')
        if(isnan(out))
            out = integral2(polarfun,-pi,pi,1,Inf,'Method','iterated','AbsTol',1e-2,'RelTol',1e-2);
            fprintf('Tolerance adjusted twice.\n')
        end
    end
else
    out = f(x, alpha);
end

end