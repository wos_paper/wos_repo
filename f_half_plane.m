function [ out ] = f_half_plane( x, alpha )
% [out]=f_half_plane(x,alpha)
%
% Exterior function: Indicator function of lower half-plane with boundary
% x(2)=-1 (vectorised)
% Used to calculate p(alpha,2) and E[N]=1/p
% See Corolarry 4.4 and Section 7 of paper
%
% Inputs
% x: point(s) in R^n for evaluation
% alpha: stability index (Laplace exponent is params.alpha/2)

[n,~]=size(x);
out=(x(n,:) < -1);

end

