function [mean_val, var_sample, mean_balls, err_est, no_samples, no_balls]=fn_run_wos_hist(x0, params, tol, method, max_samples, no_per_batch, method_params, fn_exact)
% [mean_val, var_sample, mean_balls, err_est, no_samples]=fn_run_wos(x0, params, tol, method, max_samples, no_per_batch, method_params, fn_exact)
%
% Driver routine for Walk-on-spheres algorithm
%
% Inputs
% x0: initial position
% params.alpha: stability index (Laplace exponent is params.alpha/2)
% params.rhs: source term (right-hand side) function
% params.ext: exterior term function
% params.d: distance to the domain-boundary function
% tol: tolerance (for Monte Carlo)
% method: method parameter (0=no rhs; 1=Monte Carlo; 2=trapezioum)

% no_per_batch=64; % batch the samples
% no_samples=1e7 % total samples

%max_samples=1e4;
%no_per_batch=400;
%no_samp_r=1e3;
%no_samp_theta=10;

if (method==1)
    no_samp_r = method_params(1);
    no_samp_theta = no_samp_r;
elseif (method==2)
    no_samp_r = method_params(1);
    no_samp_theta = method_params(2);
else
    no_samp_r = 0;
    no_samp_theta = 0;
end

tic
% initialise
sum_val=zeros(1,2);
sum_sq_val=zeros(1,2);
mean_val=zeros(1,2);
mean_sq_val=zeros(1,2);
var_sample=zeros(1,2);
sum_balls=0;
no_balls = zeros(1,max_samples);

err_est=[tol+1,tol+1];
num=[0,0];
k=0;
while err_est(1)>(tol/2) && err_est(2)>(tol/2) && num(1)<max_samples,
    num(1)=num(1)+1;
    [valf, valg, sum_balls_batch, no_balls_batch]=fn_wos_core_estimation(x0, params,...
        no_per_batch, method, no_samp_r, no_samp_theta);
    no_balls((k+1):(k+no_per_batch)) = no_balls_batch;
    sum_val(1)=sum_val(1)+mean(valf);
    sum_val(2)=sum_val(2)+mean(valg);
    sum_sq_val(1)=sum_sq_val(1)+mean(valf.^2);
    sum_sq_val(2)=sum_sq_val(2)+mean(valg.^2);
    sum_balls=sum_balls+sum_balls_batch;
    mean_val  =sum_val/num(1); % mean at exit point
    mean_sq_val = sum_sq_val/num(1);
    var_sample =mean_sq_val-mean_val.^2; % sample variance at exit point
    no_samples=no_per_batch*num(1);
    k=k+no_per_batch;
    err_est=sqrt(var_sample/no_samples);
    fprintf('%8.3f - %8.3f\n',var_sample(2), err_est(2))
    if num(1) == 1
        toc
    end
end
num(2)=num(1);
if (err_est(2)<=(tol/2) && err_est(1)>(tol/2) && num(1)<max_samples)
    method = 0;
    while err_est(1)>(tol/2) && num(1)<max_samples,
        num(1)=num(1)+1;
        [valf, ~, sum_balls_batch, no_balls_batch]=fn_wos_core_estimation(x0, params,...
            no_per_batch, method, no_samp_r, no_samp_theta);
        no_balls((k+1):(k+no_per_batch)) = no_balls_batch;
        sum_val(1)=sum_val(1)+mean(valf);
        sum_sq_val(1)=sum_sq_val(1)+mean(valf.^2);
        sum_balls=sum_balls+sum_balls_batch;
        mean_val(1)  =sum_val(1)/num(1); % mean at exit point
        mean_sq_val(1) = sum_sq_val(1)/num(1);
        var_sample(1) =mean_sq_val(1)-mean_val(1)^2; % sample variance at exit point
        no_samples=no_per_batch*num(1);
        k=k+no_per_batch;
        err_est(1)=sqrt(var_sample(1)/no_samples);
    end
elseif (err_est(1)<=(tol/2) && err_est(2)>(tol/2) && num(2)<max_samples)
    while err_est(2)>(tol/2) && num(2)<max_samples,
        num(2)=num(2)+1;
        [~, valg, sum_balls_batch]=fn_wos_core_estimation(x0, params,...
            no_per_batch, method, no_samp_r, no_samp_theta);
        no_balls((k+1):(k+no_per_batch)) = no_balls_batch;
        sum_val(2)=sum_val(2)+mean(valg);
        sum_sq_val(2)=sum_sq_val(2)+mean(valg.^2);
        sum_balls=sum_balls+sum_balls_batch;
        mean_val(2)  =sum_val(2)/num(2); % mean at exit point
        mean_sq_val(2) = sum_sq_val(2)/num(2);
        var_sample(2) =mean_sq_val(2)-mean_val(2)^2; % sample variance at exit point
        no_samples=no_per_batch*num(2);
        k=k+no_per_batch;
        err_est(2)=sqrt(var_sample(2)/no_samples);
        fprintf('%8.3f - %8.3f\n',var_sample(2), err_est(2))
    end
end
toc
no_samples = no_per_batch * num;
mean_balls=sum_balls/max(no_samples); % mean no. balls to exit
%
if (nargin>7)
    exact=fn_exact(x0,params.alpha) % exact value
    error=sum(mean_val)-exact
    tol
end

end
