#   [Unbiased walk-on-spheres Monte Carlo methods for the fractional Laplacian](http://arxiv.org/abs/1609.03127) 
##  Andreas E. Kyprianou, Ana Osojnik, and  Tony Shardlow  

This is a MATLAB implementation of the algorithms described in the above paper:


>
We consider Monte Carlo methods for simulating solutions to the analogue of the  Dirichlet problem in which the Laplacian is replaced by the fractional Laplacian. Specifically we consider the analogue of the so-called walk-on-spheres algorithm
  in the diffusive setting. This entails sampling the path of Brownian motion as it
  uniformly exits a sequence of spheres maximally inscribed in the domain on which the
  Dirichlet problem is defined. As this algorithm would otherwise never end, it is
  truncated when the walk-on-spheres comes within epsilon of the boundary.
  In the setting of the fractional Laplacian, the role of Brownian motion is replaced
  by an isotropic stable process. A significant difference to the Brownian setting is
  that the stable processes will exit spheres by a jump rather than hitting their
  boundary. This difference ensures that, unlike the diffusive setting, the algorithm
  ends after an almost surely finite number of steps which does not depend on the point
  of issue of the stable process

### CODE USER GUIDE

Main exemplary running scripts are:

- ```run_wos_test``` : main example routine which finds an estimate u(x0) up to a certain tolerance or after some maximum number of Monte Carlo samples
- ```run_wos_alpha``` : routine to find estimates u(x0) for different values of alpha and plot results
- ```run_path_2d```
- ```run_path_3d```

To run own examples of problems one needs to write functions params.d, params.g, params.f and exact (optional if solution to be compared) which specify the Dirichlet problem. These function are then input into fn_run_wos script which outputs an appropriate estimate along with other information about the sample set. Below are some crucial instructions for writing these functions.

Instructions for writing functions specifying the Dirichlet problem:

- ```params.d``` : function to compute the shortest distance to the domain boundary, preferably vectorised, takes ```(x)``` as input, negative distance means the point of inquiry is outside the domain
- ```params.ext``` : exterior condition function, must be vectorised, takes ```(x,alpha)``` as input, zero exterior function is ```f0```
- ```params.rhs``` : source-term function, must be vectorised, takes ```(x,alpha)``` as input
- ```exact``` : solution (comparison) function, preferably vectorised, takes ```(x,alpha)``` as input

Additional notes:

- If any of your functions require additional arguments as those above, input them as for example: ```exact = @(x,alpha) u(x,alpha,arg1,arg2,...)```
- See functions ```d_ball_0_1```, ```f1```, ```f2```, ```g3```, ```g4```, ```u3```, ```u4``` for example functions
- See scripts run_wos_test or run_wos_alpha for correct input of newly written functions
- ```f0``` and ```g0``` are zero exterior and source-term functions, respectively
- ```u_hom_check``` : returns the numerical approximation of the homogenous part of the solution, to specify the Dirichlet problem ```params.rhs=@g0``` should be used in this case and ```params.ext``` arbitrary