function [z]=sample_path(y0,alpha,N,epsilon)
% function [z]=sample_path(y0,alpha,N,epsilon)
%
% Generate a sample path for alpha-stable Levy process
% with N points, based on exits of spheres of radius epsilon
%
% Input:
% y0: d-vector for initial position
% alpha: Levy parameter
% N: number of points
% epsilon: radius
%
% Output:
% z: (N,d) matrix of positions (d is length of y0)
%
d=length(y0); z=zeros(N,d); z(1,:)=y0; y=y0(:);
if d==2,
  sample=@() uniform2d();
end
if d==3,
  sample=@() uniform3d();
end
if d>3.
  sample=@() uniformany(d);
end
for i=2:N,
  r1 = cdfinv(rand,alpha,epsilon);
  y = y + r1*sample();
  z(i,:)=y';
end;


function out=uniform2d()
  theta=2*pi*rand;
  out= [cos(theta);sin(theta)];
end;

%
function out=uniform3d()
  h=-1+2*rand; theta=2*pi*rand; r=sqrt(1-h*h);
  out=[r*cos(theta); r*sin(theta); h];
end
%
function X=uniformany(d)
  X=2*ones(d,1);
  while norm(X)>1,
    X=2*rand(d,1)-1;
  end;
end
%
end
