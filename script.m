params.alpha=1.8 % Levy parameter
Matern.q=1+params.alpha % Matern regularity
Matern.ell=0.2% Matern length scale
%
no_its=1000
no_samples_in=100 % min number of samples
%grid_delta=0.2
no_mc_quad=100 % for rhs MC
%
%prob_num=1; % zero external
prob_num=1; % zero rhs
%
if prob_num==1,
  params.f=@f0; % exterior
  params.g=@g4; % rhs
  params.d=@d_ball_0_1;
  params.exact=@u4;
  params.xy=[-1,1,-1,1];
  params.dx=0.1;
else
  N=3;
  params.f=@f0;%zero
  params.g=@g3;%constant
  params.d=@(R)swiss_cheese_vectorised(R,N-1);
  params.xy=[-N,N,-N,N];
  params.exact=@u4;% none available
  params.dx=0.1;
end
%
close all;
tic
[mu,C,grid]=wos_test_field( Matern, grid_delta, no_samples_in, no_mc_quad, no_its, params);
toc
[~,h] = contourf(grid.x,grid.y,mu,10);
set(h,'LineColor','none')
colorbar
delta=params.dx^2;
l2_err=sqrt(sum(C(:))*delta)
%
