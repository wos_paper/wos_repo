function [ out ] = f2( x, alpha )
% [out]=f2(x,alpha,varargin)
%
% Exterior function: Gaussian with P (vectorised)
% See Section 7, Example 2 of paper
% Solution (numerical) for this problem with rhs=0 is u_hom_check
%
% Inputs
% x: point(s) in R^n for evaluation
% alpha: stability index (Laplace exponent is params.alpha/2)

P=[2;0]; % any point outside ball

[~,m]=size(x);
out=exp(-sum((x-repmat(P,[1,m])).^2));

end
